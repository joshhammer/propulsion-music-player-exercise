
class Player {

    constructor(){
        this.tracks = [];
        this.counter = 0;
    }

    addTrack(trackInfo){
        this.tracks.push(trackInfo);
    }

    play(){
        console.log(`Now playing: '${this.tracks[this.counter].title}' by '${this.tracks[this.counter].artist}'`)
    }

    playTrackNo(num){
        console.log(`Now playing: '${this.tracks[num].title}' by '${this.tracks[num].artist}'`)
        this.counter = num;
    }

    playAll(){
        this.trackIntervalTime = 3000;

        this.play();
        this.next();

        this.trackInterval = setInterval(() => {
            this.play();
            this.next();
        }, this.trackIntervalTime);

        setTimeout(() => {
            this.stop();
        }, (this.tracks.length - 2)*this.trackIntervalTime);
    }

    stop(){
        clearInterval(this.trackInterval);
    }

    next(){
        if(this.counter === (this.tracks.length - 1) ){
            this.counter = 0;
        }
        else {
            this.counter++;
        }
    }

    previous(){
        if(this.counter === 0){
            this.counter = (this.tracks.length - 1)
        }
        else {
            this.counter--;
        }
    }

    printAllTracks(){
        for(let item of this.tracks){
            console.log(item);
        }
    }
}

class Track {
    constructor(artist, title, album){
        this.artist = artist;
        this.title = title;
        this.album = album;
    }

    print(){
        console.log(
            `Artist: ${this.artist}\nTitle: ${this.title}\nAlbum: ${this.album}`
        )
    }
}

const player = new Player();

const nirvanaTrack = new Track('Nirvana', 'In Bloom', 'Nevermind')
const radioheadTrack = new Track('Radiohead', 'Fake Plastic Trees', 'The Bends')
const nationalTrack = new Track('The National', 'Bloodbuzz Ohio', 'High Violet')
const vanEttenTrack = new Track('Sharon van Etten', 'Give Out', 'Tramp')
const arcadeFireTrack = new Track('Arcade Fire', 'Suburban War', 'The Suburbs')

player.addTrack(nirvanaTrack);
player.addTrack(radioheadTrack);
player.addTrack(nationalTrack);
player.addTrack(vanEttenTrack);
player.addTrack(arcadeFireTrack);
